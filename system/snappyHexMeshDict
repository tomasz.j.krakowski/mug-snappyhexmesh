/*--------------------------------*- C++ -*----------------------------------*\
  =========                 |
  \\      /  F ield         | OpenFOAM: The Open Source CFD Toolbox
   \\    /   O peration     | Website:  https://openfoam.org
    \\  /    A nd           | Version:  10
     \\/     M anipulation  |
\*---------------------------------------------------------------------------*/
FoamFile
{
    format      ascii;
    class       dictionary;
    object      snappyHexMeshDict;
}
// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //


castellatedMesh on;
snap            off; //trzeba wlaczyc po ustaleniu pierwszego etapu 
addLayers       off;

geometry
{
   mug
    {
        type closedTriSurfaceMesh;
        file "geom.stl";

        regions
        {
            inside {name coffee; }
            wall {name walls; }
        }
    }
};

castellatedMeshControls
{
    maxLocalCells 500000;
    maxGlobalCells 100000;
    minRefinementCells 1000;
    maxLoadUnbalance 0.1;
    nCellsBetweenLevels 3;

    features
    (
    );

    refinementSurfaces
    {
        mug
        {
            level (2 2); //dostosowanie zageszczenia siatki
            patchInfo
            {
                type patch;
            }
        }
    }

    resolveFeatureAngle 30;

    refinementRegions
    {
        /*
        walls
        {
            mode inside;
            levels ((0 4));
        }
        */
    }
    insidePoint (0.05 0.001 0.001);
    allowFreeStandingZoneFaces true;
}

snapControls
{
    // Number of patch smoothing iterations before finding correspondence
    // to surface
    nSmoothPatch 3;

    // Maximum relative distance for points to be attracted by surface.
    // True distance is this factor times local maximum edge length.
    // Note: changed(corrected) w.r.t 17x! (17x used 2* tolerance)
    tolerance 1.0;

    // Number of mesh displacement relaxation iterations.
    nSolveIter 100;

    // Maximum number of snapping relaxation iterations. Should stop
    // before upon reaching a correct mesh.
    nRelaxIter 5;

    // Feature snapping

        // Number of feature edge snapping iterations.
        // Leave out altogether to disable.
        nFeatureSnapIter 15;

        // Detect (geometric only) features by sampling the surface
        // (default=false).
        implicitFeatureSnap false;

        // Use castellatedMeshControls::features (default = true)
        explicitFeatureSnap true;

        // Detect features between multiple surfaces
        // (only for explicitFeatureSnap, default = false)
        multiRegionFeatureSnap false;
}

addLayersControls
{
    layers
    {
    }

    relativeSizes       true;
    expansionRatio      1.2;
    finalLayerThickness 0.5;
    minThickness        1e-3;
}

writeFlags
(
);

meshQualityControls
{
    //- Maximum non-orthogonality allowed. Set to 180 to disable.
    maxNonOrtho 65;

    //- Max skewness allowed. Set to <0 to disable.
    maxBoundarySkewness 20;
    maxInternalSkewness 4;

    //- Max concaveness allowed. Is angle (in degrees) below which concavity
    //  is allowed. 0 is straight face, <0 would be convex face.
    //  Set to 180 to disable.
    maxConcave 80;

    //- Minimum projected area v.s. actual area. Set to -1 to disable.
    minFlatness 0.5;

    //- Minimum cell pyramid volume relative to min bounding box length^3
    //  Set to a fraction of the smallest cell volume expected.
    //  Set to very negative number (e.g. -1e30) to disable.
    minVol -1e30;

    minTetQuality 1e-30;

    //- Minimum face twist. Set to <-1 to disable. dot product of face normal
    //  and face centre triangles normal
    minTwist 0.02;

    //- Minimum normalised cell determinant
    //  1 = hex, <= 0 = folded or flattened illegal cell
    minDeterminant 0.001;

    //- minFaceWeight (0 -> 0.5)
    minFaceWeight 0.02;

    //- minVolRatio (0 -> 1)
    minVolRatio 0.01;

    // Advanced

    //- Number of error distribution iterations
    nSmoothScale 4;
    //- Amount to scale back displacement at error points
    errorReduction 0.75;
}
mergeTolerance 1e-6;

// ************************************************************************* //
